xhost local:root
docker run \
	-it \
	--privileged \
	--net host \
	--ipc host \
	--name rosman \
	-e DISPLAY=$DISPLAY \
	-v ~/ros2_ws:/ros_ws \
	-v ~/microros_ws/:/microros_ws \
	-v ~/ros2_debug/:/ros_debug_ws \
	-v ~/ros2_ws/src/:/ros_ws/src \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
	ros_dev:Dockerfile
