# Create directories
mkdir ~/microros_ws/src
mkdir ~/ros2_debug/src  
mkdir ~/ros2_ws/src

# Build docker image
docker build -t "ros_dev:Dockerfile" .

# Run docker container with all parameters
docker run -it --privileged --net host --ipc host --name rosman -e $"DISPLAY=($env.DISPLAY)" -v $"($env.HOME)/ros2_ws:/ros_ws" -v $"($env.HOME)/microros_ws:/microros_ws" -v $"($env.HOME)/ros2_debug:/ros_debug_ws" -v $"($env.HOME)/ros2_ws/src:/ros_ws/src" -v /tmp/.X11-unix:/tmp/.X11-unix ros_dev:Dockerfile

