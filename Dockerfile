ARG ROS_DISTRO="humble"
ARG DEBIAN_FRONTEND=noninteractive
FROM ros:${ROS_DISTRO}
ENV DISPLAY $DISPLAY
# install ros2 packages

RUN apt update && apt upgrade && apt install -y --quiet --no-install-recommends \
  ros-${ROS_DISTRO}-desktop-full=0.10.0-1* \
  software-properties-common \
  && add-apt-repository universe; \
  apt update -y && apt upgrade -y && \
  apt install -y  \
  bash-completion \
  bzip2 \
  build-essential \
  clang \
  curl \
  git \
  gnupg2 \
  libclang-dev \
  libudev-dev \
  librust-alsa-sys-dev \
  librust-libudev-dev \
  lld \
  pip \
  python3-pip \
  python3-vcstool \
  python3-typeguard \
  ros-${ROS_DISTRO}-generate-parameter-library \
  ros-${ROS_DISTRO}-generate-parameter-library-py \
  ros-${ROS_DISTRO}-navigation2 \
  ros-${ROS_DISTRO}-nav2-bringup \
  ros-${ROS_DISTRO}-parameter-traits \
  ros-${ROS_DISTRO}-robot-localization \
  ros-${ROS_DISTRO}-ros2bag \
  ros-${ROS_DISTRO}-rosbag2-storage-default-plugins \
  ros-${ROS_DISTRO}-ros2-control \
  ros-${ROS_DISTRO}-ros2-controllers \
  ros-${ROS_DISTRO}-rsl \
  ros-${ROS_DISTRO}-turtlebot3-gazebo \
  ros-${ROS_DISTRO}-test-interface-files \
  wget && rm -rf /var/lib/apt/lists/* && \
  pip install --upgrade argcomplete\
  ros2-numpy \
  colcon-meson \
  colcon-gradle \
  colcon-package-selection \
  pytest \
  pypozyx \
  requests \
  git+https://github.com/colcon/colcon-common-extensions.git \
  git+https://github.com/colcon/colcon-cargo.git \
  git+https://github.com/colcon/colcon-ros-cargo.git; \
  pip3 install opengen; \
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain 1.84.0 -y && \
  . $HOME/.cargo/env && \
  rustup toolchain install nightly && \
  rustup completions bash cargo > $HOME/cargo_complete && \
  rustup component add llvm-tools-preview && \
  cargo +nightly install -Z sparse-registry --debug --no-track --verbose \
  cargo-outdated cargo-ament-build \
  echo 'source $HOME/.cargo/env; eval "$(register-python-argcomplete3 ros2)"' >> $HOME/.bashrc; \
  echo 'eval "$(register-python-argcomplete3 ros2)"' >> $HOME/.bashrc; \
  echo 'eval "$(register-python-argcomplete3 colcon)"' >> $HOME/.bashrc && \
  echo '. /opt/ros/humble/setup.sh' >> $HOME/.bashrc && \
  echo "alias rosSource='source ${HOME}/.bashrc'" >> $HOME/.bashrc; \
  echo "colcon build --continue-on-error --build-base /ros_ws/build --install-base /ros_ws/install --base-paths /ros_default /ros_ws /microros_ws" > /colcon_build.sh; \
  echo "export ROS_DOMAIN_ID=0" >> ~/.bashrc; \
  chmod 777 /colcon_build.sh;\
  echo "alias colconBuild='/colcon_build.sh'" >> $HOME/.bashrc;\
  echo "alias cargo_ament='cargo-ament-build --install-base /ros_ws/install --merge-install --symlink-install'" >> $HOME/.bashrc; \
  git clone https://github.com/ros2-rust/ros2_rust.git /ros_default/src/ros2_rust; \
  git clone -b main https://github.com/ros2-rust/rosidl_rust.git /ros_default/src/rosidl_rust; \
  git clone -b main https://github.com/ros2-rust/rosidl_runtime_rs.git /ros_default/src/rosidl_runtime_rs; \
  git clone -b ${ROS_DISTRO} https://github.com/ros2/common_interfaces.git /ros_default/src/common_interfaces; \
  git clone -b ${ROS_DISTRO} https://github.com/ros2/example_interfaces.git /ros_default/src/example_interfaces; \
  git clone -b ${ROS_DISTRO} https://github.com/ros2/rcl_interfaces.git /ros_default/src/rcl_interfaces; \
  git clone -b ${ROS_DISTRO} https://github.com/ros2/test_interface_files.git /ros_default/src/test_interface_files; \
  git clone -b ${ROS_DISTRO} https://github.com/ros2/rosidl_defaults.git /ros_default/src/rosidl_defaults; \
  git clone -b ${ROS_DISTRO} https://github.com/ros2/unique_identifier_msgs.git /ros_default/src/unique_identifier_msgs; \
  git clone https://gitlab.com/ros21923912/lanelet_msgs.git;
ENV PATH=/root/.cargo/bin:$PATH
RUN mkdir -p /ros_ws/src /microros_ws/src ros_debug_ws/src
